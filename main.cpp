#include <QCoreApplication>
#include "BIMserverConnection.hpp"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    BIMserverConnection r(nullptr, "http://localhost:8082/json");
    r.loadModel();

    return a.exec();
}
