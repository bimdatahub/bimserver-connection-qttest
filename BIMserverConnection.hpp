#ifndef BIMSERVERCONNECTION_HPP
#define BIMSERVERCONNECTION_HPP

#include <QUrl>
#include <QtWidgets/QMessageBox>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonObject>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QByteArray>

class BIMserverConnection : public QObject{
    Q_OBJECT

public:

    /// Default constructor
    BIMserverConnection(QObject * parent, const char * bimserverUrl);

    /// Virtual destructor
    ~BIMserverConnection();

    /// Load a model from BIMserver given the login credentials and BIMserver address
    void loadModel();

private:

    void loginRequest(QString username, QString password) const;

    QNetworkAccessManager* m_networkManager;
    QNetworkReply* m_networkReply;
    QUrl m_bimserverURL;
    QString m_token;

private slots:
    void processLoginRequest(QNetworkReply *rep);
    void processDownloadRequest(QNetworkReply *rep);
    void processGetDownloadDataRequest(QNetworkReply *rep);

};

#endif // BIMSERVERCONNECTION_HPP
