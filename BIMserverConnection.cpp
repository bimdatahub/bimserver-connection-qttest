#include "BIMserverConnection.hpp"
#include <iostream>
#include <QFile>
#include <QTextStream>

BIMserverConnection::BIMserverConnection(QObject * parent, const char * bimserverUrl):
    QObject(parent),
    m_networkManager(new QNetworkAccessManager)
{
    m_bimserverURL=QString(bimserverUrl);
}

BIMserverConnection::~BIMserverConnection()
{
    delete m_networkManager;
}

void BIMserverConnection::loadModel()
{
    //std::cout << "Hello World";
    QString username("admin@bimserver.org");
    QString password("admin");
    loginRequest(username, password);
}

void BIMserverConnection::loginRequest(QString username, QString password) const
{
    //construct login json
    QJsonObject parameters;
    parameters["username"] = QJsonValue(username);
    parameters["password"] = QJsonValue(password);
    QJsonObject request;
    request["interface"] = QJsonValue("Bimsie1AuthInterface");
    request["method"] = QJsonValue("login");
    request["parameters"] = parameters;
    QJsonObject loginRequest;
    loginRequest["token"] = "1";
    loginRequest["request"] = request;

    QJsonDocument doc;
    doc.setObject(loginRequest);

    QByteArray loginJson = doc.toJson();

    //setup network connection
    QNetworkRequest qNetworkRequest(m_bimserverURL);
    qNetworkRequest.setRawHeader("Content-Type", "application/json");

    // disconnect all signals from m_networkManager to this
    disconnect(m_networkManager, nullptr, this, nullptr);
    connect(m_networkManager,&QNetworkAccessManager::finished, this, &BIMserverConnection::processLoginRequest);
    m_networkManager->post(qNetworkRequest,loginJson);
}

void BIMserverConnection::processLoginRequest(QNetworkReply *rep)
{
    //extract token from login Request
    QByteArray responseArray = rep->readAll();

    QJsonDocument responseDoc = QJsonDocument::fromJson(responseArray);
    QJsonObject loginResponse = responseDoc.object();
    QJsonObject response = loginResponse["response"].toObject();
    m_token = response["result"].toString();

    //TODO Chong add find serializer and find project method

    //setup download IFC json //TODO Chong setup parameters
    QJsonObject parameters;
    parameters["roid"] = QJsonValue(65539);
    parameters["serializerOid"] = QJsonValue(1703974);
    parameters["showOwn"] = QJsonValue(false);
    parameters["sync"] = QJsonValue(false);
    QJsonObject request;
    request["interface"] = QJsonValue("Bimsie1ServiceInterface");
    request["method"] = QJsonValue("download");
    request["parameters"] = parameters;
    QJsonObject downloadRequest;
    downloadRequest["token"] = QJsonValue(m_token);
    downloadRequest["request"] = request;

    QJsonDocument doc;
    doc.setObject(downloadRequest);

    QByteArray downloadJson = doc.toJson();

    //setup network connection
    QNetworkRequest qNetworkRequest(m_bimserverURL);
    qNetworkRequest.setRawHeader("Content-Type", "application/json");

    // disconnect all signals from m_networkManager to this
    disconnect(m_networkManager, nullptr, this, nullptr);
    connect(m_networkManager,&QNetworkAccessManager::finished, this, &BIMserverConnection::processDownloadRequest);
    m_networkManager->post(qNetworkRequest,downloadJson);
}

void BIMserverConnection::processDownloadRequest(QNetworkReply *rep)
{
    //extract token from login Request
    QByteArray responseArray = rep->readAll();

    QJsonDocument responseDoc = QJsonDocument::fromJson(responseArray);
    QJsonObject downloadResponse = responseDoc.object();
    QJsonObject response = downloadResponse["response"].toObject();
    int actionId = response["result"].toInt();

    //setup get download data IFC json //TODO Chong setup parameters
    QJsonObject parameters;
    parameters["actionId"] = QJsonValue(actionId);
    QJsonObject request;
    request["interface"] = QJsonValue("Bimsie1ServiceInterface");
    request["method"] = QJsonValue("getDownloadData");
    request["parameters"] = parameters;
    QJsonObject getDownloadDataRequest;
    getDownloadDataRequest["token"] = QJsonValue(m_token);
    getDownloadDataRequest["request"] = request;

    QJsonDocument doc;
    doc.setObject(getDownloadDataRequest);

    QByteArray getDownloadDataJson = doc.toJson();

    //setup network connection
    QNetworkRequest qNetworkRequest(m_bimserverURL);
    qNetworkRequest.setRawHeader("Content-Type", "application/json");

    // disconnect all signals from m_networkManager to this
    disconnect(m_networkManager, nullptr, this, nullptr);
    connect(m_networkManager,&QNetworkAccessManager::finished, this, &BIMserverConnection::processGetDownloadDataRequest);
    m_networkManager->post(qNetworkRequest,getDownloadDataJson);
}

void BIMserverConnection::processGetDownloadDataRequest(QNetworkReply *rep)
{
    //extract token from login Request
    QByteArray responseArray = rep->readAll();

    QJsonDocument responseDoc = QJsonDocument::fromJson(responseArray);
    QJsonObject downloadResponse = responseDoc.object();
    QJsonObject response = downloadResponse["response"].toObject();
    QJsonObject result = response["result"].toObject();
    QString file = result["file"].toString();

    //decode the response
    QByteArray byteArray;
    byteArray.append(file);
    QString OSMFile = QByteArray::fromBase64(byteArray);

    //write to file
    QFile testFile("C:/testOSM.osm");
    testFile.open(QIODevice::WriteOnly | QIODevice::Text);
    QTextStream out(&testFile);
    out << OSMFile;
    testFile.close();
}
