#-------------------------------------------------
#
# Project created by QtCreator 2014-09-20T13:20:05
#
#-------------------------------------------------

QT       += core
QT       += network widgets
QT       -= gui

TARGET = QtTest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    BIMserverConnection.cpp \
    ReverseTranslator.cpp

HEADERS += \
    BIMserverConnection.hpp \
    ReverseTranslator.hpp
